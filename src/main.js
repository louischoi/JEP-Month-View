import Vue from 'vue';
import {
  Alert,
  Avatar,
  Button,
  Calendar,
  Checkbox,
  Drawer,
  Dropdown,
  Empty,
  Divider,
  notification,
  Menu,
  DatePicker,
  Modal,
  Spin,
} from 'ant-design-vue';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faBars,
  faChevronLeft,
  faChevronRight,
  faSignOutAlt,
  faExclamationTriangle,
  faInfo,
  faTimes,
  faCheck,
  faCalendarAlt,
} from '@fortawesome/free-solid-svg-icons';
import {
  faGoogle,
  faFacebook,
} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import './firebase';
import vuex from './vuex';

import App from './App.vue';
import './registerServiceWorker';

Vue.config.productionTip = false;
Vue.use(Checkbox);
Vue.use(Button);
Vue.use(Avatar);
Vue.use(Calendar);
Vue.use(DatePicker);
Vue.use(Drawer);
Vue.use(Dropdown);
Vue.use(Divider);
Vue.use(Modal);
Vue.use(Menu);
Vue.use(Alert);
Vue.use(Empty);
Vue.use(Spin);
Vue.prototype.$notification = notification;

library.add(faBars);
library.add(faChevronLeft);
library.add(faChevronRight);
library.add(faSignOutAlt);
library.add(faExclamationTriangle);
library.add(faInfo);
library.add(faTimes);
library.add(faCheck);
library.add(faCalendarAlt);
library.add(faGoogle);
library.add(faFacebook);
Vue.component('font-awesome-icon', FontAwesomeIcon);

new Vue({
  render: (h) => h(App),
  store: vuex,
}).$mount('#app');
