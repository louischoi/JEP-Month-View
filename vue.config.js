const webpack = require('webpack');
const _ = require('lodash');
const path = require('path');

function resolve(dir) {
  return path.join(__dirname, dir);
}

const isRoot = process.env.IS_ROOT;
const publicPath = ((!_.isUndefined(isRoot))
  ? ''
  : (process.env.NODE_ENV === 'production' && '/JEP-Month-View/')) || '';

module.exports = {
  lintOnSave: false,
  publicPath,
  pwa: {
    themeColor: '#8B0000',
  },
  pluginOptions: {
    webpackBundleAnalyzer: {
      openAnalyzer: false,
    },
  },
  configureWebpack: {
    plugins: [
      new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en/),
    ],
    resolve: {
      alias: {
        '@ant-design/icons/lib/dist$': resolve('./src/ant-icons.js'),
      },
    },
  },
};
