import _set from 'lodash/set';
import Vue from 'vue';
import Vuex from 'vuex';
import {
  APP_CONFIG_SET,
  USER_SET,
  FILTERS_SET,
  AVAILABILITIES_SET,
} from './mutation-types';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  strict: debug,
  plugins: [],
  state: {
    config: {},
    window: {
      isMobile: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
    },
    user: null,
    filters: [],
    availabilities: {},
  },
  mutations: {
    /* eslint-disable no-param-reassign */
    [APP_CONFIG_SET]: (state, config) => {
      _set(state, 'config', config);
    },
    [USER_SET]: (state, user) => {
      state.user = user;
    },
    [FILTERS_SET]: (state, filters) => {
      state.filters = filters;
    },
    [AVAILABILITIES_SET]: (state, availabilities) => {
      state.availabilities = availabilities;
    },
  },
});
