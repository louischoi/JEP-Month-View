import _get from 'lodash/get';
import _map from 'lodash/map';
import _isFunction from 'lodash/isFunction';
import _forEach from 'lodash/forEach';
import ErrCode from 'err-code';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/remote-config';

export const ERROR_CODE_FIREBASE_AUTH = {
  ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL: 'auth/account-exists-with-different-credential',
  // Thrown if there already exists an account with the email address asserted by the credential.
  // Resolve this by calling firebase.auth.Auth.fetchSignInMethodsForEmail with the error.email and then asking the user to sign in using one of the returned providers.
  // Once the user is signed in, the original credential retrieved from the error.credential can be linked to the user with firebase.User.linkWithCredential to prevent the user from signing in again to the original provider via popup or redirect.
  // If you are using redirects for sign in, save the credential in session storage and then retrieve on redirect
  // and repopulate the credential using for example firebase.auth.GoogleAuthProvider.credential depending on the credential provider id and complete the link.
  AUTH_DOMAIN_CONFIG_REQUIRED: 'auth/auth-domain-config-required',
  // Thrown if authDomain configuration is not provided when calling firebase.initializeApp(). Check Firebase Console for instructions on determining and passing that field.
  CANCELLED_POPUP_REQUEST: 'auth/cancelled-popup-request',
  // Thrown if successive popup operations are triggered. Only one popup request is allowed at one time. All the popups would fail with this error except for the last one.
  OPERATION_NOT_ALLOWED: 'auth/operation-not-allowed',
  // Thrown if the type of account corresponding to the credential is not enabled. Enable the account type in the Firebase Console, under the Auth tab.
  OPERATION_NOT_SUPPORTED_IN_THIS_ENVIRONMENT: 'auth/operation-not-supported-in-this-environment',
  // Thrown if this operation is not supported in the environment your application is running on. "location.protocol" must be http or https.
  POPUP_BLOCKED: 'auth/popup-blocked',
  // Thrown if the popup was blocked by the browser, typically when this operation is triggered outside of a click handler.
  POPUP_CLOSED_BY_USER: 'auth/popup-closed-by-user',
  // Thrown if the popup window is closed by the user without completing the sign in to the provider.
  UNAUTHORIZED_DOMAIN: 'auth/unauthorized-domain',
  // Thrown if the app domain is not authorized for OAuth operations for your Firebase project. Edit the list of authorized domains from the Firebase console.

  // AUTH_DOMAIN_CONFIG_REQUIRED: 'auth/auth-domain-config-required',
  // Thrown if authDomain configuration is not provided when calling firebase.initializeApp(). Check Firebase Console for instructions on determining and passing that field.
  // OPERATION_NOT_SUPPORTED_IN_THIS_ENVIRONMENT: 'auth/operation-not-supported-in-this-environment',
  // Thrown if this operation is not supported in the environment your application is running on. "location.protocol" must be http or https.
  // UNAUTHORIZED_DOMAIN: 'auth/unauthorized-domain',
  // Thrown if the app domain is not authorized for OAuth operations for your Firebase project. Edit the list of authorized domains from the Firebase console.

  APP_DELETED: 'auth/app-deleted',
  // Thrown if the instance of FirebaseApp has been deleted.
  APP_NOT_AUTHORIZED: 'auth/app-not-authorized',
  // Thrown if the app identified by the domain where it's hosted, is not authorized to use Firebase Authentication
  // with the provided API key. Review your key configuration in the Google API console.
  ARGUMENT_ERROR: 'auth/argument-error',
  // Thrown if a method is called with incorrect arguments.
  INVALID_API_KEY: 'auth/invalid-api-key',
  // Thrown if the provided API key is invalid. Please check that you have copied it correctly from the Firebase Console.
  INVALID_USER_TOKEN: 'auth/invalid-user-token',
  // Thrown if the user's credential is no longer valid. The user must sign in again.
  INVALID_TENANT_ID: 'auth/invalid-tenant-id',
  // Thrown if the tenant ID provided is invalid.
  NETWORK_REQUEST_FAILED: 'auth/network-request-failed',
  // Thrown if a network error (such as timeout, interrupted connection or unreachable host) has occurred.
  // OPERATION_NOT_ALLOWED: 'auth/operation-not-allowed',
  // Thrown if you have not enabled the provider in the Firebase Console.
  // Go to the Firebase Console for your project, in the Auth section and the Sign in Method tab and configure the provider.
  REQUIRES_RECENT_LOGIN: 'auth/requires-recent-login',
  // Thrown if the user's last sign-in time does not meet the security threshold.
  // Use firebase.User.reauthenticateWithCredential to resolve. This does not apply if the user is anonymous.
  TOO_MANY_REQUESTS: 'auth/too-many-requests',
  // Thrown if requests are blocked from a device due to unusual activity.
  // Trying again after some delay would unblock.
  // UNAUTHORIZED_DOMAIN: 'auth/unauthorized-domain',
  // Thrown if the app domain is not authorized for OAuth operations for your Firebase project.
  // Edit the list of authorized domains from the Firebase console.
  USER_DISABLED: 'auth/user-disabled',
  // Thrown if the user account has been disabled by an administrator.
  // Accounts can be enabled or disabled in the Firebase Console, the Auth section and Users subsection.
  USER_TOKEN_EXPIRED: 'auth/user-token-expired',
  // Thrown if the user's credential has expired. This could also be thrown if a user has been deleted.
  // Prompting the user to sign in again should resolve this for either case.
  WEB_STORAGE_UNSUPPORTED: 'auth/web-storage-unsupported',
  // Thrown if the browser does not support web storage or if the user disables them.
};

export const ERROR_CODE_FIRESTORE = {
  // The operation was cancelled (typically by the caller).
  CANCELLED: 'cancelled',
  UNKNOWN: 'unknown',
  //  Client specified an invalid argument. Note that this differs from 'failed-precondition'.
  //  'invalid-argument' indicates arguments that are problematic
  //  regardless of the state of the system (e.g. an invalid field name).
  INVALID_ARGUMENT: 'invalid-argument',
  //  Deadline expired before operation could complete.
  //  For operations that change the state of the system,
  //  this error may be returned even if the operation has completed successfully.
  //  For example, a successful response from a server could have been delayed long enough
  //  for the deadline to expire.
  DEADLINE_EXCEEDED: 'deadline-exceeded',
  //  Some requested document was not found.
  NOT_FOUND: 'not-found',
  // Some document that we attempted to create already exists.
  ALREADY_EXISTS: 'already-exists',
  // The caller does not have permission to execute the specified operation.
  PERMISSION_DENIED: 'permission-denied',
  // Some resource has been exhausted, perhaps a per-user quota,
  // or perhaps the entire file system is out of space.
  RESOURCE_EXHAUSTED: 'resource-exhausted',
  // Operation was rejected because the system is not in a
  // state required for the operation's execution.
  FAILED_PRECONDITION: 'failed-precondition',
  // The operation was aborted, typically due to a concurrency issue like transaction aborts, etc.
  ABORTED: 'aborted',
  // Operation was attempted past the valid range.
  OUT_OF_RANGE: 'out-of-range',
  // Operation is not implemented or not supported/enabled.
  UNIMPLEMENTED: 'unimplemented',
  // Internal errors. Means some invariants expected by underlying system has been broken.
  // If you see one of these errors, something is very broken.
  INTERNAL: 'internal',
  // The service is currently unavailable. This is most likely a transient condition
  // and may be corrected by retrying with a backoff.
  UNAVAILABLE: 'unavailable',
  // Unrecoverable data loss or corruption.
  DATA_LOSS: 'data-loss',
  // The request does not have valid authentication credentials for the operation.
  UNAUTHENTICATED: 'unauthenticated',
};

export const ERROR_CODE_CONFIG = {
  FORBIDDEN_VERSION: 'forbidden/version',
  FORBIDDEN_MAINTENANCE: 'forbidden/maintenance',
};

export const AUTH_PROVIDERS = {
  GOOGLE: firebase.auth.GoogleAuthProvider,
  FACEBOOK: firebase.auth.FacebookAuthProvider,
};

function extractUserInfo(firebaseUser) {
  return _get(firebaseUser, 'providerData.0', null);
}

async function signIn(Provider, isMobile = false) {
  if (!_isFunction(Provider)) {
    return undefined;
  }

  const providerInstance = new Provider();
  firebase.auth().useDeviceLanguage();

  const result = isMobile
    ? await firebase.auth().signInWithRedirect(providerInstance)
    : await firebase.auth().signInWithPopup(providerInstance);
  // // This gives you a Google Access Token. You can use it to access the Google API.
  // const token = result.credential.accessToken;
  // // The signed-in user info.
  // const user = result.user;
  return extractUserInfo(result.user);
}

export default {
  setUpRemoteConfig: async () => {
    try {
      const remoteConfig = firebase.remoteConfig();
      remoteConfig.settings = {
        fetchTimeoutMillis: 10000,
        minimumFetchIntervalMillis: 10000, // FIXME
      };
      remoteConfig.defaultConfig = ({
        MODE_MAINTENANCE: false,
        AUTH_PROVIDERS: [],
        ANNOUNCEMENTS: [],
        ROUTES_CONFIG: [],
        UI_PRECONDITION_VERSION: '',
      });
      await remoteConfig.fetchAndActivate();
      return remoteConfig;
    } catch (e) {
      console.error(e);
      throw e;
    }
  },
  assertRemoteConfig: async (config) => {
    if (config.MODE_MAINTENANCE) {
      throw ErrCode(new Error('Site in maintenance mode.'), ERROR_CODE_CONFIG.FORBIDDEN_MAINTENANCE);
    }
  },

  registerOnAuthStateChanged: (callback) => firebase.auth().onAuthStateChanged((firebaseUser) => {
    callback(extractUserInfo(firebaseUser));
  }),
  signInViaGoogle: async (isMobile) => signIn(AUTH_PROVIDERS.GOOGLE, isMobile),
  signInViaFacebook: async (isMobile) => signIn(AUTH_PROVIDERS.FACEBOOK, isMobile),
  signOut: async () => {
    await firebase.auth().signOut();
  },

  getAvailabilities: async () => {
    const firestoreInstance = firebase.firestore();
    const collectionRef = firestoreInstance.collection('routes');

    const { docs } = await collectionRef.where('subscribed', '==', true).get();
    const routes = _map(docs, (doc) => doc.data());
    const filters = _map(routes, ({ from, to }) => ({
      key: `${from}-${to}`,
      show: true,
    }));
    const availabilities = {
    };

    _forEach(routes, ({ from, to, availability }) => {
      const label = `${from}-${to}`;
      _forEach(availability, ({ date, options }) => {
        const key = (new Date(date.seconds * 1000)).toDateString();
        if (!availabilities[key]) {
          availabilities[key] = {
            [label]: [],
          };
        }
        availabilities[key][label] = options;
      });
    });
    return {
      filters,
      availabilities,
    };
  },
};
